#encoding: utf-8

Given(/^I create a new subtask called "Todo New SubTask"$/) do
  value = "//a[@href='/tasks']"
  text = "My Tasks"
  content = @page.find_element_by("xpath", value).text
  if text.match(content)
    puts "Link: #{content} on the navbar"
  else
    fail "There is no Link: #{content} on the navbar"
  end
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  value =  "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'Todo New Task')]"
  task = @check.check_task_name("xpath", value, 'Todo New Task')
  @create.task_pressing_enter('id', 'new_task', task)
  value = "//button[@class='btn btn-xs btn-primary ng-binding' and contains(text(), 'Manage Subtasks')]"
  @page.click_element_by("xpath", value)
  value = "//a[@editable-text='sub_task.body']"
  subtaskname = @check.check_task_name("xpath", value, 'Todo New SubTask')
  value = "//input[@id='new_sub_task']"
  @create.create_subtask("xpath", value, subtaskname)
end

Then(/^I should see my "Todo New SubTask" on the list of created subtasks$/) do
  value =  "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'Todo New Task')]"
  task = @check.check_task_count("xpath", value, 'Todo New Task')
  taskname = 'Todo New Task'+task.to_s
  content = @page.find_element_by("xpath", value).text
    if taskname.match(content)
      puts "Created Subtask: #{content}"
    else
      fail "Subtask: #{content} wasn't created"
    end
end

Given(/^I try to create a new empty subtask$/) do
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  value =  "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'Todo New Task')]"
  task = @check.check_task_name("xpath", value, 'Todo New Task')
  @create.task_pressing_enter('id', 'new_task', task)
  value = "//button[@class='btn btn-xs btn-primary ng-binding' and contains(text(), 'Manage Subtasks')]"
  @page.click_element_by("xpath", value)
  value = "//input[@id='new_sub_task']"
  @create.create_subtask_empty("xpath", value)
end

Then(/^I shouldn't see an empty subtask on the list of created tasks$/) do
  value = "//a[@class='ng-scope ng-binding editable editable-click editable-empty' and contains(text(), 'empty')]"
  element = @page.find_element_by("xpath", value)
  if element.text == 'empty'
    value = "//button[@ng-click='removeSubTask(sub_task)' and contains(text(), 'Remove SubTask')]"
    elements = @page.find_elements_by("xpath", value)
    element = elements[0]
    element.click
    puts "Task empty deleted"
    fail "Error => Shouldn't see empty task created on the list of created tasks !"
  else
    puts "Empty task wasn't created. It's OK!"
  end
end

Given(/^I try to create a subtask with more than 250 characters$/) do
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  value =  "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'Todo New Task')]"
  task = @check.check_task_name("xpath", value, 'Todo New Task')
  @create.task_pressing_enter('id', 'new_task', task)
  value = "//button[@class='btn btn-xs btn-primary ng-binding' and contains(text(), 'Manage Subtasks')]"
  @page.click_element_by("xpath", value)
  value = "//input[@id='new_sub_task']"
  subtask = @check.check_task_random(127)
  @create.create_subtask("xpath", value, subtask)

end

Then(/^I shouldn't create a subtask with 250 characters$/) do
  task = @check.check_task_random(127)
  value = "//a[@editable-text='sub_task.body']"
  element = @page.find_elements_by("xpath", value)
  if element[0].text.length > 250
    fail "Error => Shouldn't create subtask with more than 250 characters!"
  else
    puts "Subtask with more than 250 characters wasn't created. It's OK!"
  end
end
