#encoding: utf-8

Given(/^that I am on ToDo App home$/) do
  @home.openSite 'http://qa-test.avenuecode.com'
end

Given(/^I create a new task called "Todo New Task"$/) do
  value = "//a[@href='/tasks']"
  text = "My Tasks"
  content = @page.find_element_by("xpath", value).text
  if text.match(content)
    puts "Link: #{content} on the navbar"
  else
    fail "There is no Link: #{context} on the navbar"
  end
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  value =  "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'Todo New Task')]"
  task = @check.check_task_name("xpath", value, 'Todo New Task')
  @create.task_pressing_enter('id', 'new_task', task)
end

Then(/^I should see my "Todo New Task" on the list of created tasks$/) do
  value =  "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'Todo New Task')]"
  task = @check.check_task_count("xpath", value, 'Todo New Task')
  taskname = 'Todo New Task'+task.to_s
  content = @page.find_element_by("xpath", value).text
    if taskname.match(content)
      puts "Created subtask: #{content}"
    else
      fail "Subtask: #{content} wasn't created"
    end
end

Given(/^I try to create a new empty task$/) do
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  value =  "//a[@class='ng-scope ng-binding editable editable-click']"
  value = "//span[@class='input-group-addon glyphicon glyphicon-plus']"
  @create.create_task_empty("xpath", value)
end

Then(/^I shouldn't see an empty task on the list of created tasks$/) do
  value =  "//a[@class='ng-scope ng-binding editable editable-click']"
  value = "//a[@class='ng-scope ng-binding editable editable-click editable-empty' and contains(text(), 'empty')]"
  element = @page.find_element_by("xpath", value)
  if element.text == 'empty'
    value = "//button[@ng-click='removeTask(task)' and contains(text(), 'Remove')]"
    elements = @page.find_elements_by("xpath", value)
    element = elements[0]
    element.click
    puts "Task empty deleted"
    fail "Error => Shouldn't see empty task created on the list of created tasks !"
  else
    puts "Empty task wasn't created. It's OK!"
  end
end

Given(/^I try to create a task with three characters$/) do
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  value =  "//a[@class='ng-scope ng-binding editable editable-click']"
  task = "AB"
  @create.task_pressing_enter('id', 'new_task', task)

end

Then(/^I shouldn't see a three characters task on the list of created tasks$/) do
  value = "//a[@class='ng-scope ng-binding editable editable-click' and contains(text(), 'AB')]"
  element = @page.find_element_by("xpath", value)
  if element.text.length < 3
    value = "//button[@ng-click='removeTask(task)' and contains(text(), 'Remove')]"
    elements = @page.find_elements_by("xpath", value)
    element = elements[0]
    element.click
    puts "Task with less than three characters deleted"
    fail "Error => Shouldn't create task with less than three characters!"
  else
    puts "Task with less than three characters wasn't created. It's OK!"
  end
end

Given(/^I try to create a task with more than 250 characters$/) do
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
  task = @check.check_task_random(127)
  value =  "//a[@class='ng-scope ng-binding editable editable-click']"
  @create.task_pressing_enter('id', 'new_task', task)
end

Then(/^I shouldn't create a task with 250 characters$/) do
  task = @check.check_task_random(127)
  value = "//a[@class='ng-scope ng-binding editable editable-click']"
  element = @page.find_elements_by("xpath", value)
  if element[0].text.length > 250
    value = "//button[@ng-click='removeTask(task)' and contains(text(), 'Remove')]"
    elements = @page.find_elements_by("xpath", value)
    element = elements[0]
    element.click
    puts "Task with more than 250 characters deleted"
    fail "Error => Shouldn't create task with more than 250 characters!"
  else
    puts "Task with more than 250 characters wasn't created. It's OK!"
  end
end
