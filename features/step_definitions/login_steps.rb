#encoding: utf-8

Given(/^I logged in with a valid user$/) do
  @login.login_ok
  @page.click_element_by("xpath", "//a[@class='btn btn-lg btn-success']")
end

Then(/^I should see "Hey Ricardo França, this is your todo list for today:"$/) do
  value = "//div[@class='container']/h1"
  text = "Hey Ricardo França, this is your todo list for today:"
  content = @page.find_element_by("xpath", value).text
  if text.match(content)
    puts "Displayed Message is correct: #{content}"
  else
    fail "Displayed message does not meet acceptance criteria! #{content}"
  end

end
