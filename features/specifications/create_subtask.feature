Feature: Create Task
As a ToDo App user
I should be able to create a subtask
So I can break down my tasks in smaller pieces

Background: Todo App Home
    Given that I am on ToDo App home

Scenario: Create a new subtask
    Given I create a new subtask called "Todo New SubTask"
    Then I should see my "Todo New SubTask" on the list of created subtasks

Scenario: Try to create an empty subtask
  Given I try to create a new empty subtask
  Then I shouldn't see an empty subtask on the list of created tasks

Scenario: Try to create a subtask with more than 250 characters
  Given I try to create a subtask with more than 250 characters
  Then I shouldn't create a subtask with 250 characters
