Feature: Login
As a ToDo App user
I should be able to sign in
So I can manage my tasks

Background: Todo App Home
    Given that I am on ToDo App home

Scenario: Login OK
    Given I logged in with a valid user
    Then I should see "Hey Ricardo França, this is your todo list for today:"
