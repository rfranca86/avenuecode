Feature: Create Task
As a ToDo App user
I should be able to create a task
So I can manage my tasks

Background: Todo App Home
    Given that I am on ToDo App home

Scenario: Create a new task
    Given I create a new task called "Todo New Task"
    Then I should see my "Todo New Task" on the list of created tasks

Scenario: Try to create an empty task
  Given I try to create a new empty task
  Then I shouldn't see an empty task on the list of created tasks

Scenario: Try to create a task with less than three characters
  Given I try to create a task with three characters
  Then I shouldn't see a three characters task on the list of created tasks

Scenario: Try to create task with more than 250 characters
  Given I try to create a task with more than 250 characters
  Then I shouldn't create a task with 250 characters
