require 'rubygems'
require 'selenium-webdriver'
# require 'securerandom'

Before do
  @driver = Selenium::WebDriver.for :firefox
  @driver.manage.window.maximize
  @driver.manage.timeouts.implicit_wait = 10
  @driver.manage.timeouts.script_timeout = 10
  @driver.manage.timeouts.page_load = 30
  @page=Page.new(@driver)
  @home=Home.new(@driver)
  @login=Login.new(@driver)
  @check=Check.new(@driver)
  @create=CreateTask.new(@driver)
end

After do |scenario|
  @driver.quit
end

#After do
#    @driver.quit
#end
