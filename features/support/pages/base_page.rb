# encoding: utf-8
class Page
  def initialize(driver)
    @driver = driver
  end

  def click_element_by (type, value)
    find_element_by(type, value).click
  end

  def input_to_element_with (type, value, input)
    input_element=find_element_by(type, value)
    input_element.clear
    input_element.send_keys input
  end

  def find_element_by (type, value)
    element=@driver.find_element type.to_sym => value
  end

  def find_elements_by (type, value)
    begin
      wait = Selenium::WebDriver::Wait.new(:timeout => 5)
      elements=wait.until{@driver.find_elements(type.to_sym => value)}
    rescue
      elements=[]
    end
  end
end
