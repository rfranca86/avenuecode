class Check < Page
  def check_text(type, value, text)
    content = find_element_by(type.to_sym, value).text
    if text.match(content)
      puts "'Text '#{text}' match with '#{content}'"
    else
      fail "there is no match for #{text}" if content != text
    end
  end

  def check_task_name(type, value, taskname)
    tasks = find_elements_by(type, value)
    token = tasks.count()+1
    return taskname+token.to_s
  end

  def check_task_count(type, value, taskname)
    tasks = find_elements_by(type, value)
    token = tasks.count()

  end

  def check_all_task_count(type, value)
    tasks = find_elements_by(type, value)
    total = tasks.count()
    return total
  end

  def check_task_random(value)
    task = SecureRandom.hex(value).to_s
  end
end
