class CreateTask < Page
  def task_pressing_enter(type, value, task)
    newtask = find_element_by(type, value)
    newtask.send_keys(task)
    newtask.send_keys(:return)
  end

  def create_task_empty(type, value)
    newtask = click_element_by(type, value)
  end

  def create_subtask(type, value, subtaskname)
    subtask = find_element_by(type, value)
    subtask.send_keys(subtaskname)
    subtask.send_keys(:return)
  end

  def create_subtask_empty(type, value)
    click = click_element_by(type, value)
    subtask = find_element_by(type, value)
    subtask.send_keys(:return)
  end
end
