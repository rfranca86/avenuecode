# README #

Avenue Code automated tests

### What is this repository for? ###

* Avenue Code selection process


### These tests were validate with: ###

* Firefox 45.0 and Firefox 49.0.1
*Selenium-webdriver 2.53.2
* Cucumber 2.4.0
* Ruby 2.3.1

### Directory Structure ###
features/specifications: contains the .features files with BDD scnearios
features/step_definition: contains the steps that reads the BDD scenarios
features/support/pages: contains the page objects to manipulate page elements
features/support/: contains the env file responsible for test environment


### Executing Automated tests ###
* Creating Task:
   cucumber features/specifications/create_task.feature -f pretty -f html -o create_task_report.html

* Creating Subtask:
   cucumber features/specifications/create_subtask.feature -f pretty -f html -o create_subtask_report.html

* Login test:
   cucumber features/specifications/login.feature -f pretty -f html -o create_subtask_report.html

   
### Login/Password to access http://qa-test.avenuecode.com ###
Login: universius@gmail.com / password: avenuecode
